# proof concept for me:
when you send a message using POST to server
the server is relay the message to amqp cloud
while server is consume the message too
the message that received from consumer will processed
and send back to frontend who connect to server using websocket

# why using mq and websocket at same time ?
from previous reading and researching, we are supposedly access the amqp server directly from our server, it's because the amqp is expensive one that mean you don't need to access the amqp from your client, moreover it's relate to security issues since your amqp username and password more vulnerable to end user.
using a server, might access the amqp directly and while for users we let them access the server thru the REST and Websocket as callback.

### Send Message:
```
POST http://localhost:3000/send
body: {
	"pyl": {
		"rcv": "useriddarireceiver",
		"typ": "t",
		"cnt": "halo apa kabar"
	}
}
headers: {
  "Content-Type": "application/json",
  "x-headers-key": "SOMETHING"
}

// better in your work, change the header key with more safe and generated key

note:
- pyl: payload
- rcv: receiver -> target pesan
- typ: type -> t / text, i / image, v / video
- cnt: content -> t / text, i / url (uploaded of image), v / url (uploaded of video)
```

### Headers Key:
Use header `x-headers-key` from env to get tru middleware check
```env
X_HEADERS_KEY="SOMETHING"
CLOUDAMQP_URL="amqps://user:password@rmq.cloudamqp.com/yours"
```

### Current Flow:
User A who access the web page (testsocket.io.html) will generate some id
User B who send a POST request (imagine, its sending a message to user) via REST
User B message will be processed via amqp / rabbitmq on the cloud
User B message from mq server will be consumed on server then pass to websocket
User A will be receive a message from websocket

Since User A using the ID

### Plan:
While server collecting the message and save to log (database)

### Future:
- Can sell a saas with this
- Can use microservices using amqp
- As alternative to kafka which need more resources than this