require("dotenv").config();
const express = require("express");
const cors = require('cors');
const { createServer } = require("http");
const { Server } = require("socket.io");

const app = express();

const AuthMiddleware = (req, res, next) => {
  // temporary using headers with same key
  const key = req.headers["x-headers-key"];

  if (key === process.env.X_HEADERS_KEY) {
    // fake user_id
    req.user = "999";

    return next();
  }

  return res.status(500).send("Invalid header");
}

const router = express.Router();

router.post("/send", (req, res) => {
  const { pyl } = req.body;
  const payload = { user: req.user, ...pyl }

  sendMessage("", "jobs", new Buffer.from(JSON.stringify(payload)));

  res.send("message sent, please check console log");
})

app.use(cors())
app.use(express.json())
app.use(express.static('public'))
app.use("/", AuthMiddleware, router);

const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "*"
  }
});

const consumer = require("./amqp/consumer");
const { start: runPublisher, publish: sendMessage } = require("./amqp/publisher");

const PORT = process.env.PORT || 3000;

// socketio
let clients = [];
io.sockets.on("connection", socket => {
  socket.on("storeClientInfo", data => {
    const info = {
      customId: data.customId,
      clientId: socket.id,
    }
    clients.push(info);
    console.log("new client is connected " + data.customId)
  });

  socket.on("disconnect", data => {
    for (let i = 0; i < clients.length; ++i) {
      let c = clients[i];
      if (c.clientId == socket.id) {
        clients.splice(i, 1);
        break;
      }
    }
    console.log("disconnect data", data);
  });
});

httpServer.listen(PORT, () => {
  consumer({ io });
  runPublisher();

  console.log("Listen on port " + PORT)
});