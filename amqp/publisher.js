require("dotenv").config()
const amqp = require("amqplib/callback_api");

const { CLOUDAMQP_URL } = process.env;
let amqpConn = null;

const start = () => {
  amqp.connect(CLOUDAMQP_URL + "?heartbeat=60", (err, conn) => {
    if (err)  {
      console.error("[AMQP]", err.message);
      return setTimeout(start, 1000);
    }
    conn.on("error", err => {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", () => {
      console.error("[AMQP] reconnecting");
      return setTimeout(start, 1000);
    });
    console.log("[AMQP] connected");
    amqpConn = conn;

    // start publisher
    publisher();
  })
}

let pubChannel = null;
let offlinePubQueue = [];
const publisher = () => {
  amqpConn.createConfirmChannel((err, ch) => {
    if (closeOnErr(err)) return;
    ch.on("error", err => {
      console.error("[AMQP] channel error", err.message);
    });
    ch.on("close", () => {
      console.log("[AMQP] channel closed");
    });

    pubChannel = ch;
    while (true) {
      let m = offlinePubQueue.shift();
      if (!m) break;
      publish(m[0], m[1], m[2]);
    }
  });
}

const publish = (exchange, routingKey, content) => {
  try {
    pubChannel.publish(
      exchange,
      routingKey,
      content,
      {
        persistent: true
      },
      (err, ok) => {
        if (err) {
          console.error("[AMQP] publish", err);
          offlinePubQueue.push([exchange, routingKey, content]);
          pubChannel.connection.close();
        }
      },
    );
  } catch (e) {
    console.error("[AMQP] publish", e.message);
    offlinePubQueue.push([exchange, routingKey, content]);
  }
}

const closeOnErr = err => {
  if (!err) return false;
  console.error("[AMQP] error", err);
  amqpConn.close();
  return true;
}

module.exports = { start, publish }