require("dotenv").config()
const amqp = require("amqplib/callback_api");
const { now } = require("mongoose");

const { CLOUDAMQP_URL } = process.env;
let amqpConn = null;

const consumer = ({ io }) => {
  amqp.connect(CLOUDAMQP_URL + "?heartbeat=60", (err, conn) => {
    if (err)  {
      console.error("[AMQP]", err.message);
      return setTimeout(consumer, 1000);
    }
    conn.on("error", err => {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", () => {
      console.error("[AMQP] reconnecting");
      return setTimeout(consumer, 1000);
    });
    console.log("[AMQP] connected");
    amqpConn = conn;

    // worker will translate message
    startWorker({ io });
  })
}

const startWorker = ({ io }) => {
  amqpConn.createChannel((err, ch) => {
    if (closeOnErr(err)) return;
    ch.on("error", err => {
      console.error("[AMQP] channel error", err.message);
    });

    ch.on("close", () => {
      console.log("[AMQP] channel closed");
    });

    ch.prefetch(10);
    ch.assertQueue(
      "jobs",
      {
        durable: true
      },
      (err, _ok) => {
        if (closeOnErr(err)) return;
        ch.consume("jobs", processMsg, { noAck: false });
        console.log("Worker is started");
      }
    );

    const processMsg = (msg) => {
      work(msg, ok => {
        try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        } catch (e) {
          closeOnErr(e);
        }
      }, io);
    }
  });
}

const work = (msg, cb, io) => {
  const isJSON = JSON.parse(msg.content.toString());
  let msgType = "text"
  switch(isJSON.typ) {
    case "t": msgType = "text"; break;
    case "i": msgType = "image"; break;
    case "v": msgType = "video"; break;
    default:
      msgType = "text";
  }
  const o = {
    from: isJSON.user,
    fromData: {
      name: ["Me","Ibun","Hawa","Sarah"][Math.floor(Math.random() * 4)],
    },
    to: isJSON.rcv,
    message: isJSON.cnt,
    type: msgType,
    postDate: now()
  }
  cb(true);
  const emit = io.emit("000CustomIdHere0000", o);
  console.log(emit, o)
}

const closeOnErr = err => {
  if (!err) return false;
  console.error("[AMQP] error", err);
  amqpConn.close();
  return true;
}

module.exports = consumer;